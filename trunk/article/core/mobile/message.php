﻿<?php
global $_W, $_GPC;
if($_GPC['isajax'] == 'true'){
	$data = array(
		'article_id'      => $_GPC['article_id'],
		'uniacid'         => $_W['uniacid'],
		'uid'             => $_W['member']['uid'],
		'status'          => $_GPC['status'],
		'article_title'   => $_GPC['article_title'],
		'message_content' => $_GPC['js_cmt_input'],
		'createtime'    => time()
	);
	
	$result = pdo_insert('ewei_shop_article_message', $data);
	if (!empty($result)) {
		$arr = array(
			'code' => 200,
			'msg' => '留言成功，将为您跳转到文章页面'
		);
		die(json_encode($arr));
	}else{
		$arr = array(
			'code' => 202,
			'msg' => '系统错误 请重试！'
		);
		die(json_encode($arr));
	}
	
}
$aid = intval($_GPC['aid']);
if (!empty($aid)) {
	$article = pdo_fetch("SELECT * FROM " . tablename('ewei_shop_article') . " WHERE id=:aid and article_state=1 and uniacid=:uniacid limit 1 ", array(':aid' => $aid, ':uniacid' => $_W['uniacid']));
	if (!empty($article)) {
		include $this->template('message');
	} else {
		die('没有查询到文章信息！请检查URL后重试！');
	}
} else {
	die('url参数错误！');
}